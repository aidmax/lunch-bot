import random
import requests
import asyncio
import json
import time
import os
import logging
from os import environ
import datetime

from telethon import Button, TelegramClient, events

from context import Context
from places import get_random_lunch_place, get_random_lunch_places

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

api_id = environ.get('API_ID')
api_hash = environ.get('API_HASH')
bot_token = environ.get('BOT_TOKEN')

bot = TelegramClient('bot_session', api_id, api_hash).start(bot_token=bot_token)

context = Context()
votes = {}

lunch_gif = ['https://media.giphy.com/media/7e3Y9b3nTWsaA/giphy.gif',
             'https://media.giphy.com/media/jKaFXbKyZFja0/giphy.gif',
             'https://media.giphy.com/media/FgNmKgaqixS3m/giphy.gif',
             'https://media.giphy.com/media/PS1Ko2AFVTso8/giphy.gif',
             'https://media.giphy.com/media/Cx2qDJYjwq1ji/giphy.gif',
             'https://media.giphy.com/media/eH3Ra3DUp3tMylXedo/giphy.gif',
             'https://media.giphy.com/media/h8uD16sumSaRi/giphy.gif',
             'https://media.giphy.com/media/APJ8aylgEPfzy/giphy.gif',
             'https://media.giphy.com/media/WKUSlerPGZ73i/giphy.gif',
             'https://media.giphy.com/media/xThuWcu8nwBhazm0Fi/giphy.gif',
             'https://media.giphy.com/media/lYRFF6Voh4brq/giphy.gif',
             'https://media.giphy.com/media/rVhabuN8Ww2NW/giphy.gif']

commands = 'Доступные команды:\n' + '\n'.join(['/lunch - Колесо фортуны',
                                               '/list - Куда ходим',
                                               '/poll - Народовластие',
                                               '/hell - Вы действительно хотите знать?',
                                               '/add place - Добавить место',
                                               '/del place - Удалить место',
                                               '/ping - Пациент скорее жив, чем мертв'])

logger.info('bot data is up')


def get_random_gif():
    return random.choice(lunch_gif)


@bot.on(events.CallbackQuery)
async def my_cb_handler(event):
    logging.info(f'Got {event}')
    data = event.data.decode()
    cb = data[data.find('_') + 1:]
    await globals()[cb](event)


async def ynm(event):
    pass


async def yesno(event):
    global votes
    v = votes.get(str(event.message_id), {})
    yes = 0
    no = 0
    data = event.data.decode()
    v[event.sender_id] = data[:data.find('_')]
    for _, val in v.items():
        if _ not in ['place', 'date'] and val == 'yes':
            yes += 1
        elif _ not in ['place', 'date'] and val == 'no':
            no += 1

    if abs(yes - no) > 2:
        ct = context.data.get(str(event.chat_id))
        ct['places'][v['place']]['votes'].append(v)
        context.data[str(event.chat_id)] = ct
        context.save()
        if yes > no:
            msg = 'Принято :)'
        else:
            msg = 'Как хотите, моё дело предложить...'
        await event.edit(msg, buttons=None)
    else:
        yes_cap = f'👍 {yes}'
        no_cap = f'👎 {no}'
        markup = bot.build_reply_markup([Button.inline(yes_cap, data='yes_yesno'), Button.inline(no_cap, data='no_yesno')])
        await event.edit(buttons=markup)
    

@bot.on(events.NewMessage())
async def my_event_handler(event):
    logging.info(f'Got {event}')


@bot.on(events.NewMessage(pattern='/vote'))
async def my_vote_handler(event):
    markup = bot.build_reply_markup([Button.inline('yes', data='yes_ynm'), 
                                     Button.inline('no', data='no_ynm'),
                                     Button.inline('maybe', data='maybe_ynm')])
    await bot.send_message(event.chat_id, 'click me', buttons=markup)


@bot.on(events.NewMessage(pattern='/lunch'))
async def my_lunch_handler(event):
    global votes
    logger.info(f'Got the command {event.raw_text} from {event.chat_id}')
    ct = context.data.get(str(event.chat_id))
    if ct:
        await bot.send_file(event.chat_id, get_random_gif())
        await asyncio.sleep(1)
        await bot.send_message(event.chat_id, 'Никаких голосований, все решаю я!')
        await asyncio.sleep(1)
        place = get_random_lunch_place(ct.get('places'))
        await bot.send_message(event.chat_id, 'Вот куда идем сегодня: **' + place + '**')

        await asyncio.sleep(1)
        markup = bot.build_reply_markup([Button.inline('👍', data='yes_yesno'),
                                         Button.inline('👎', data='no_yesno')])
        res = await bot.send_message(event.chat_id, 'Ну как вам? Пойдёте?', buttons=markup)
        v = votes.get(str(res.id), {})
        v['place'] = place
        v['date'] = str(datetime.date.today())
        votes[str(res.id)] = v
    else:
        await bot.send_message(event.chat_id, 'Нет данных')


@bot.on(events.NewMessage(pattern=r'/add(@\w+)?\s(.+)'))
async def add_request_handler(event):
    place = event.pattern_match[2]
    ct = context.data.get(str(event.chat_id))
    if not ct:
        ct = {'places': {}}
    if place not in ct['places'].keys():
        ct['places'][place] = {
            'votes': []
        }
        await event.reply(f'Место {place} добавлено')
    else:    
        await event.reply(f'Место {place} уже есть в списке, обормот!')
    context.data[str(event.chat_id)] = ct
    context.save()


@bot.on(events.NewMessage(pattern=r'/del(@\w+)?\s(.+)'))
async def del_request_handler(event):
    place = event.pattern_match[2]
    ct = context.data.get(str(event.chat_id))
    if not ct:
        ct = {'places': {}}
    if place in ct['places'].keys():
        del ct['places'][place]
        await event.reply(f'Место {place} удалено')
    else:    
        await event.reply(f'Места {place} нет в списке, обормот!')
    context.data[str(event.chat_id)] = ct
    context.save()


@bot.on(events.NewMessage(pattern='/list'))
async def list_request_handler(event):
    await bot.send_message(event.chat_id, 
                           'Вот, что у меня есть:\n' + '\n'.join(
                               context.data.get(str(event.chat_id)).get('places')
                            )
                           )


@bot.on(events.NewMessage(pattern='/poll'))
async def launch_poll(event):
    logger.info('launching poll')
    ct = context.data.get(str(event.chat_id))
    random_places = get_random_lunch_places(ct.get('places'), 4)
    logger.info(f'Random places: {random_places}')
    url = 'https://api.telegram.org/bot%s/sendPoll' % bot_token
    payload = {'chat_id': event.chat_id,
               'question': 'Куда?',
               'options': random_places,
               'is_anonymous': False,
               'allows_multiple_answers': True}
    r = requests.post(url, data=json.dumps(payload), headers={'Content-type': 'application/json'})
    logger.info('status code: %d' % r.status_code)
    logger.info(r.text)
    await bot.send_message(event.chat_id, 'Голосуем!')
    await bot.send_file(event.chat_id, get_random_gif())


@bot.on(events.NewMessage(pattern='/hell'))
async def tram_place_message(event):
    await bot.send_file(event.chat_id, 'https://media.giphy.com/media/n0pwZRRYfLv7q/giphy.gif')
    await bot.send_message(event.chat_id, 'Трамвайка')


@bot.on(events.NewMessage(pattern='/info'))
async def send_info(event):
    await bot.send_message(event.chat_id, commands)


@bot.on(events.NewMessage(pattern='/ping'))
async def ping(event):
    await bot.send_message(event.chat_id, 'Рано вы меня хороните')


logger.info('start')

def main():
    bot.run_until_disconnected()


if __name__ == '__main__':
    main()
