import json
import logging


class Singleton(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Singleton, cls).__new__(cls)
        return cls.instance

class Context(Singleton):
    data = {}

    def __init__(self):
        try:
            with open('context.json') as context_file:
                self.data = json.load(context_file)
        except FileNotFoundError:
            self.data = {}

        # CONVERT OLD SCTRUCTURE
        for chat, chat_data in self.data.items():
            if isinstance(chat_data['places'], list):
                logging.info(f'Converting old data structure for chat {chat}...')
                places = {}
                for place in chat_data['places']:
                    places[place] = {
                        'votes': []
                    }
                chat_data['places'] = places
                logging.info('Done')
        self.save()


    def save(self):
        with open('context.json', 'w') as context_file:
                json.dump(self.data, context_file)
