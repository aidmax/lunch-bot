import random
# ['Корейцы', 'Сербы', 'Лукоморье', 'Розенбаум', 'Бурито', 'Вьетнам', 'Вилка', 'Барвинок', 'Слойка']


def get_random_lunch_place(places):
    return random.choice(list(places))


def get_random_lunch_places(places, number=4):
    return random.sample(list(places), k=len(places))[:number if number <= len(places) else len(places)]
